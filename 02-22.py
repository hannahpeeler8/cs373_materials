# -----------
# Wed, 20 Feb
# -----------

"""
Some users of Netflix:
	Netflix
	GE
	Capital One
	NASA
	McDonalds
"""

"""
Short overview of Services discussed:
	RDS - Relational Database Service
	EC2 - Elastic Cloud Compute
	IAM - Identity and Access Management
	S3 - Simple Storage Service
	Route 53 - DNS
	EB - Elastic Beanstalk
	ACM - AWS Certificate Manager
"""

"""
Some general notes
    - When following tutorials, be thorough and read EVERYTHING (even before you begin).
      AWS and GCP alike are complicated tools and seemingly innocuous config
      setting can completely change the overall behavior of an instance
    - For phase 1, I might suggest jumping to "Deploy a React App on Amazon S3..."
      later in these notes. It's quick and gets you what you need
    - START EARLY!!! It's easy for things to go wrong in cloud computing, and you
      won't have fancy support packages that large companies do. The TAs will help when we 
      can, but no one has all the answers.
"""

"""
Tutorial for creating RDS database:
    https://aws.amazon.com/getting-started/tutorials/create-connect-postgresql-db/
    
    The above tutorial is better than many others I've found and includes
    additional guidance on connecting to an instance once it's created.
    All default values they specify are the same as what I did in class,
    and are different
"""

"""
Deploying a React App:
    https://medium.com/ovrsea/deploy-automatically-a-react-app-on-amazon-s3-iam-within-minutes-da6cb0096d55
    
    This one is quick, but there are some weird steps because S3 can get weird,
    feel free to ask questions.
    This tutorial touches on two other tools: CloudFront and IAM.
    I went over IAM in class, but not cloudfront. Cloudfront is a tool for
    static content delivery and you will only really use for S3 because
    it lets you use HTTPS!

    Common problem: You get 403 errors because some of your S3 bucket settings are off
    If that happens, follow this post and find where the "Public Access Settings" are
    being changed. Follow that
    https://medium.freecodecamp.org/how-to-host-a-website-on-s3-without-getting-lost-in-the-sea-e2b82aa6cd38
"""

"""
Enabling HTTPS for your website
    Some notes about using https:
        https://developers.google.com/web/fundamentals/security/encrypt-in-transit/why-https
        
    Using Cloudfront to enable https for S3:
        https://medium.com/ovrsea/deliver-your-react-app-in-milliseconds-with-cloudfront-fd3a2d038445
        
        Cloudfront does the SSL Certificate in the background, you'll have to
        explicitly link it for other tools such as EB
        
    Adding https to EB:
        https://hackernoon.com/how-to-set-up-https-for-your-domain-on-aws-8f771686603d
        
        This is where you end up using ACM, that part is straightforward
        
    You can't use https with all services on AWS, make sure that you're using at least
    one of the tools from this link in your final solution for each phase:
    https://docs.aws.amazon.com/acm/latest/userguide/acm-services.html
"""

"""
Side notes and anecdotes
    - When I worked at Intel, AWS went down one day (HUGE fluke) and since
      1. Canvas uses AWS in the backend and 2. My work management software used it
      too, all of a sudden I was completely incapable of doing ANY work. I've never
      seen so many people go down for a coffee break at the same time, and likely never will.
"""
